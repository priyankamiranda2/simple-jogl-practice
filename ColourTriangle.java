package graphics_programs;

import com.jogamp.opengl.GLAutoDrawable;

import java.io.File;

import javax.swing.JFrame;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
//import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

 
public class ColourTriangle implements GLEventListener { 
	File file;
	Texture texture;
   
   @Override 
   public void display( GLAutoDrawable drawable ) { 
   
	   GL2 gl2 = drawable.getGL().getGL2(); // The object that contains all the OpenGL methods.
       
       gl2.glClear( GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT );
       
       gl2.glLoadIdentity();             // Set up modelview transform. 
       

      
   } 
   
   @Override 
   public void dispose( GLAutoDrawable drawable ) { 
      //method body 
   } 
   
   @Override 
   public void init( GLAutoDrawable drawble ) { 
	   try {                
	        File f=new
	        File("C:\\Users\\Priyanka Miranda\\workspace1\\graphics_programs\\src\\graphics_programs\\t\\flower.jpg");
	        texture=TextureIO.newTexture(f,true);
	       } catch (Exception ex) {
	    	   System.out.println("Exception");
	       }  

   	GL2 gl2 = drawable.getGL().getGL2();
       gl2.glMatrixMode(GL2.GL_PROJECTION);
       gl2.glOrtho(-1, 1 ,-1, 1, -1, 1);
       gl2.glMatrixMode(GL2.GL_MODELVIEW);
       gl2.glClearColor( 0, 0, 0, 1 );
       gl2.glEnable(GL2.GL_DEPTH_TEST);
     //  gl2.glEnable(GL2.GL_TEXTURE_2D);
       texture.enable(gl2);
       texture.bind(gl2);
       texture.setTexParameteri(gl2,GL2.GL_TEXTURE_WRAP_S,GL2.GL_REPEAT);
     	texture.setTexParameteri(gl2,GL2.GL_TEXTURE_WRAP_T,GL2.GL_REPEAT);

	
   }
 
   @Override 
   public void reshape( GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4 ) { 
      // method body 
   } 
   
   public static void main( String[] args ) {    
   
      //getting the capabilities object of GL2 profile
      final GLProfile profile = GLProfile.get( GLProfile.GL2 ); 
      GLCapabilities capabilities = new GLCapabilities(profile);
          
      // The canvas  
      final GLCanvas glcanvas = new GLCanvas( capabilities ); 
      ColourTriangle triangle = new ColourTriangle(); 
      glcanvas.addGLEventListener( triangle ); 
      glcanvas.setSize( 400, 400 );   
      
      //creating frame 
      final JFrame frame = new JFrame (" Colored Triangle"); 
          
      //adding canvas to it 
      frame.getContentPane().add( glcanvas ); 
      frame.setSize( frame.getContentPane().getPreferredSize()); 
      frame.setVisible( true ); 
      
   } //end of main
	
} //end of class


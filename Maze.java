package graphics_programs;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

@SuppressWarnings("serial")
public class Maze extends JPanel implements GLEventListener {
	File file;
	Texture texture;
	int x=10,y=10,width=10,height=10,a=10;
	public static void main(String args[]){
		  JFrame window = new JFrame("The Amazing maze");
		  final GLProfile profile = GLProfile.get(GLProfile.GL2);
	        GLCapabilities capabilities = new GLCapabilities(profile);
	        final GLCanvas glcanvas = new GLCanvas(capabilities);
	        TextureToPolygon cube = new TextureToPolygon();
	        glcanvas.addGLEventListener(cube);        
	        glcanvas.setSize(400,400);
	        window.add( glcanvas );
	      window.pack();
	      window.setLocation(100,100);
	      window.setResizable(true);
	     window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      window.setVisible(true);	
	      glcanvas.requestFocusInWindow();
	}
	private void Maze1(GL2 gl2,int a,int b,int c){
		 gl2.glClearColor( 0, 0, 0, 0);

	      JButton button1= new JButton("Left!");
	  	button1.addActionListener(new ActionListener(){
	  		public void actionPerformed(ActionEvent e){	
	  		x=x-10;
	  		repaint();	
	  		}
	  	});
	  	button1.setBounds(300,10,80,20);
	  	add(button1);
	  	 JButton button2= new JButton("Right!");
		  	button2.addActionListener(new ActionListener(){
		  		public void actionPerformed(ActionEvent e){
		  			x=x+10;
		  		repaint();	
		  		}
		  	});
		  	button2.setBounds(400,10,80,20);
		  	add(button2);
		  	 JButton button3= new JButton("Up!");
			  	button3.addActionListener(new ActionListener(){
			  		public void actionPerformed(ActionEvent e){
			  			
				  		y=y-10;
				  		
			  		repaint();	
			  		}
			  	});
			  	button3.setBounds(300,40,80,20);
			  	add(button3);
			  	 JButton button4= new JButton("Down!");
				  	button4.addActionListener(new ActionListener(){
				  		public void actionPerformed(ActionEvent e){
				  
					  		y=y+10;
					  		
				  		repaint();	
				  		}
				  	});
				  	button4.setBounds(400,40,80,20);
				  	add(button4);
	}

	 public void display(GLAutoDrawable drawable) {    
	        
	        GL2 gl2 = drawable.getGL().getGL2(); // The object that contains all the OpenGL methods.
	        gl2.glClear( GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT );
	        gl2.glLoadIdentity();             // Set up modelview transform. 
            Maze1(gl2,1,1,1);
	        
	    } // end display()

	    public void init(GLAutoDrawable drawable) {
	    	try {                
		        File f=new
		        File("C:\\Users\\Priyanka Miranda\\workspace1\\graphics_programs\\src\\graphics_programs\\t\\maze.jpg");
		        texture=TextureIO.newTexture(f,true);
		       } catch (Exception ex) {
		    	   System.out.println("Exception");
		       }
	    	GL2 gl2 = drawable.getGL().getGL2();
//	        gl2.glMatrixMode(GL2.GL_PROJECTION);
//	        gl2.glOrtho(-1, 1 ,-1, 1, -1, 1);
//	        gl2.glMatrixMode(GL2.GL_MODELVIEW);
//	        gl2.glClearColor( 0, 0, 0, 1 );
//	        gl2.glEnable(GL2.GL_DEPTH_TEST);
	        texture.enable(gl2);
	        texture.bind(gl2); 
	      	
	    }

	    public void dispose(GLAutoDrawable drawable) {
	            // called when the panel is being disposed
	    }

	    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
	            // called when user resizes the window
	    }

}

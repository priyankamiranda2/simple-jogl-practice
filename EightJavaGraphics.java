package graphics_programs;

import java.awt.EventQueue;
import javax.swing.JFrame;


@SuppressWarnings("serial")
public class EightJavaGraphics extends JFrame {

    public EightJavaGraphics() {

        add(new TexturedCube());
        
        setResizable(false);
        pack();
        
        setTitle("Snake");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    

    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {                
                JFrame ex = new EightJavaGraphics();
                ex.setVisible(true);                
            }
        });
    }
}
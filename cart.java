package graphics_programs;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.glu.GLU;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import com.jogamp.opengl.util.FPSAnimator;

import com.jogamp.opengl.util.gl2.GLUT;//import

public class cart  implements GLEventListener 
{

    //public static DisplayMode dm, dm_old;
    private GLU glu = new GLU();
   // private GLU glu1 = new GLU();// add before psvm()
    GLUT glut=new GLUT();
  private float rquad = 0.0f;
 private float k=-4f;
float i=0f;
  
    public void fn(GL2 gl)
    {
    	//translation
    	//gl.glPushMatrix();
    	//float rtri=0.0f;
    	//gl.glTranslatef(k, 0f, -3f );            
    		//k=k+0.01f;
    	//bus(gl);
    	//gl.glRotatef(rtri,0f,0f,1f);
    	//drawFilledCircle1(gl);
    	//drawFilledCircle2(gl);
    	//rtri+=0.2f;
    	//gl.glPopMatrix();
    	bus(gl);

    	//grass
    	 gl.glTranslatef(-0.5f, -1f, -9f );
    	 gl.glBegin(GL2.GL_POLYGON);
         gl.glColor3f(0.0f,0.6f, 0f);
        // gl.glVertex3f(-11f,1.5f,0f);//A
         gl.glVertex3f(-11f,-4.5f,0f);//E
         gl.glVertex3f(13f,-4.5f,0f);//F
         gl.glVertex3f(13f,1.8f,0f);//G
         gl.glVertex3f(-11f,1.8f,0f);//A
         gl.glEnd();
         
         
         //road
    	 gl.glBegin(GL2.GL_POLYGON);
         gl.glColor3f(0.8f,0.6f, 0.2f);
         gl.glVertex3f(-11f,-1.0f,0f);//A
         gl.glVertex3f(-11f,0.8f,0f);
         
         gl.glVertex3f(13f,0.8f,0f);//G
         gl.glVertex3f(13f,-1.0f,0f);//F
         gl.glEnd();
        
     	
    	
    }
    public void display(GLAutoDrawable drawable) 
    {
    	float ts=-0.5f;
    	
        final GL2 gl = drawable.getGL().getGL2();
        gl.glClear( GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT ); //clear all pixels
        gl.glLoadIdentity();
        gl.glTranslatef(ts, -1f, -6f );  
        fn(gl);


	            

  	//buildings
     //first
gl.glTranslatef(2f, 2f, 2f);
gl.glScalef(2f,2f,2f);
	    gl.glBegin(GL2.GL_POLYGON);
	    gl.glColor3f(0.1f,0.1f, 0.1f);
	    gl.glVertex2f(-4.3f,-0.1f);//G
	    gl.glVertex2f(-4.3f,2f);//F
	    gl.glColor3f(0.0f,0.0f, 0f);
	    gl.glVertex2f(-3.5f,2f);//E
	    gl.glVertex2f(-3.5f,-0.1f);//A
	    gl.glEnd();
	  //windows1
        int loop1;
   	 float x=-4.1f;
   	 float y=1.8f;
        for(loop1=0;loop1<6;loop1++)
        {
            gl.glBegin(GL2.GL_POLYGON);
            gl.glColor3f(1f,1f, 0f);
            gl.glVertex2f(x,y);//G
            gl.glVertex2f(x+0.1f,y);//F
            gl.glVertex2f(x+0.1f,y-0.1f);//A
            gl.glVertex2f(x,y-0.1f);//E
            gl.glEnd();
       	 
            y=y-0.3f;
        }
      //windows2
        int loop2;
   	 float x1=-3.7f;
   	 float y1=1.8f;
        for(loop2=0;loop2<6;loop2++)
        {
            gl.glBegin(GL2.GL_POLYGON);
            gl.glColor3f(1f,1f, 0f);
            gl.glVertex2f(x1,y1);//G
            gl.glVertex2f(x1+0.1f,y1);//F
            gl.glVertex2f(x1+0.1f,y1-0.1f);//A
            gl.glVertex2f(x1,y1-0.1f);//E
            gl.glEnd();
       	 
            y1=y1-0.3f;
        }
	    
	    //second
	    gl.glBegin(GL2.GL_POLYGON);
	    gl.glColor3f(0.05f,0.05f, 0.05f);
	    gl.glVertex2f(-3.5f,-0.1f);//G
	    gl.glVertex2f(-3.5f,1.8f);//F
	    gl.glColor3f(0.0f,0.0f,0f);
	    gl.glVertex2f(-3f,1.8f);//E
	    gl.glVertex2f(-3f,-0.1f);//A
	    gl.glEnd();
	  //windows3
        int loop4;
   	 float x3=-3.4f;
   	 float y3=1.6f;
        for(loop4=0;loop4<5;loop4++)
        {
            gl.glBegin(GL2.GL_POLYGON);
            gl.glColor3f(1f,1f, 0f);
            gl.glVertex2f(x3,y3);//G
            gl.glVertex2f(x3+0.1f,y3);//F
            gl.glVertex2f(x3+0.1f,y3-0.1f);//A
            gl.glVertex2f(x3,y3-0.1f);//E
            gl.glEnd();
       	 
            y3=y3-0.3f;
        }
      //windows4
        int loop3;
   	 float x2=-3.2f;
   	 float y2=1.6f;
        for(loop3=0;loop3<5;loop3++)
        {
            gl.glBegin(GL2.GL_POLYGON);
            gl.glColor3f(1f,1f, 0f);
            gl.glVertex3f(x2,y2,0f);//G
            gl.glVertex3f(x2+0.1f,y2,0f);//F
            gl.glVertex3f(x2+0.1f,y2-0.1f,0f);//A
            gl.glVertex3f(x2,y2-0.1f,0f);//E
            gl.glEnd();
            y2=y2-0.3f;
        }

	    
	    drawFilledCircle3(gl);
     //moon
    // gl.glColor3f(0.7f, 0.7f, 0.8f);
     
		//	gl.glTranslatef(1f, 1.3f, 0.0f);
		  //  glut.glutSolidSphere (0.3, 20, 10); //create ball.

				   
     }
    public void bus(GL2 gl)
    {

         	//front
    	gl.glPushMatrix();
    	float rtri=0.0f;
    	//for(i=0f;i<15f;i++)
    	gl.glTranslatef(k, 0f, -3f );            
    		k=k+0.01f;
    	gl.glRotatef(rtri,0f,0f,1f);
    	drawFilledCircle1(gl);
    	drawFilledCircle2(gl);
    	rtri+=0.2f;
         	gl.glBegin(GL2.GL_POLYGON);
             gl.glColor3f(1.0f,0.0f, 0.0f);
             gl.glVertex2f(-8f,1.8f);//G
             gl.glVertex2f(-8f,0f);//F
             
             gl.glVertex2f(-5.5f,0f);//A
             gl.glVertex2f(-5.5f,1.8f);//E
             gl.glEnd();

             //right
             gl.glBegin(GL2.GL_POLYGON);
             gl.glColor3f(1f,0.3f, 0.3f);
             gl.glVertex2f(-5.5f,1.8f);//G
             gl.glVertex2f(-5.5f,0f);//F
             gl.glColor3f(1f,0.0f, 0.0f);
             gl.glVertex2f(-5.3f,0.2f);//A
             gl.glVertex2f(-5.3f,2f);//E
             
             gl.glEnd();
             //top
             gl.glBegin(GL2.GL_POLYGON);
             gl.glColor3f(1f,0.3f, 0.3f);
             gl.glVertex2f(-8f,1.8f);//G
             gl.glVertex2f(-5.5f,1.8f);//F
             gl.glColor3f(1f,0.0f, 0.0f);
             gl.glVertex2f(-5.3f,2f);//A
             gl.glVertex2f(-7.8f,2f);//E
             gl.glEnd();
             
             //windows
             int loop;
        	 float x=-7.9f;
        	 float y=1.5f;
             for(loop=0;loop<5;loop++)
             {
                 gl.glBegin(GL2.GL_POLYGON);
                 gl.glColor3f(0.0f,0.0f, 0.7f);
                 gl.glVertex3f(x,y,0f);//G
                 gl.glVertex3f(x+0.3f,y,0f);//F
                 gl.glVertex3f(x+0.3f,y-0.3f,0f);//A
                 gl.glVertex3f(x,y-0.3f,0f);//E
                 gl.glEnd();
            	 
                 x=x+0.5f;
                 
                 //line1
                 gl.glBegin(GL2.GL_POLYGON);
                 gl.glColor3f(1.0f,1.0f, 1.0f);
                 gl.glVertex3f(-7.9f,0.8f,0f);//G
                 gl.glVertex3f(-6.2f,0.8f,0f);//F
                 gl.glVertex3f(-6.2f,0.68f,0f);//A
                 gl.glVertex3f(-7.9f,0.68f,0f);//E
                 gl.glEnd();
                 
                 //line2
                 gl.glBegin(GL2.GL_POLYGON);
                 gl.glColor3f(1.0f,1.0f, 1.0f);
                 gl.glVertex3f(-7.9f,0.62f,0f);//G
                 gl.glVertex3f(-6.2f,0.62f,0f);//A
                 gl.glVertex3f(-6.2f,0.5f,0f);//F
                 gl.glVertex3f(-7.9f,0.5f,0f);//E
                 gl.glEnd();
                 
             }
             
             //door
             gl.glBegin(GL2.GL_POLYGON);
             gl.glColor3f(0.0f,0.0f, 0.7f);
             gl.glVertex3f(-5.7f,0.9f,0f);//G
             gl.glVertex3f(-5.7f,0f,0f);//F
             gl.glVertex3f(-6.0f,0f,0f);//A
             gl.glVertex3f(-6.0f,0.9f,0f);//E
             gl.glEnd();
             gl.glPopMatrix();
             
             
    }
             //back wheels 1 
             public void drawFilledCircle1(GL2 gl)
             {
            		float i;
            		float triangleAmount = 16f; //# of triangles used to draw circle
            		
            		//GLfloat radius = 0.8f; //radius
            		float twicePi = 2.0f * 3.142f;
            		
            		gl.glBegin(GL2.GL_TRIANGLE_FAN);
                    gl.glColor3f(0.2f,0.2f, 0.2f);
            			gl.glVertex3f(-7.5f, -0.32f,0f); // center of circle
            			for(i = 0f; i <= triangleAmount;i++) 
            			{ 
            				float c=(float) (0.38f * Math.cos(i *  twicePi / triangleAmount));
            				float s=(float)(0.38f * Math.sin(i * twicePi / triangleAmount));
            				gl.glVertex3f(-7.5f +c,  -0.32f +s,0f );
            			}
            		gl.glEnd();
            	}
         
             //front wheels
             public void drawFilledCircle2(GL2 gl)
             {
            		float i;
            		float triangleAmount = 16f; //# of triangles used to draw circle
            		
            		//GLfloat radius = 0.8f; //radius
            		float twicePi = 2.0f * 3.142f;
            		
            		gl.glBegin(GL2.GL_TRIANGLE_FAN);
                    gl.glColor3f(0.2f,0.2f, 0.2f);
            			gl.glVertex3f(-6.0f, -0.32f,0f); // center of circle
            			for(i = 0; i <= triangleAmount;i++) { 
            				gl.glVertex3f(
            			            -6.0f + (float)(0.38f * Math.cos(i *  twicePi / triangleAmount)), 
            				    -0.32f + (float)(0.38f * Math.sin(i * twicePi / triangleAmount)),0f
            				);
            			}
            		gl.glEnd();
            	}

    	     public void drawFilledCircle3(GL2 gl)
    	     {
    	    		float i;
    	    		float triangleAmount = 16f; //# of triangles used to draw circle
    	    		
    	    		//GLfloat radius = 0.8f; //radius
    	    		float twicePi = 2.0f * 3.142f;
    	    		
    	    		gl.glBegin(GL2.GL_TRIANGLE_FAN);
    	    		gl.glTranslatef(3f, 10f, 8f);
    	    		gl.glScalef(2f, 2f, 2f);
    	            gl.glColor3f(1f,1f, 1f);
    	    			gl.glVertex3f(1, 1.3f,0f); // center of circle
    	    			for(i = 0; i <= triangleAmount;i++) { 
    	    				gl.glVertex3f(
    	    			            1f + (float)(0.38f * Math.cos(i *  twicePi / triangleAmount)), 
    	    				    1.3f + (float)(0.38f * Math.sin(i * twicePi / triangleAmount)),0f
    	    				);
    	    			}
    	    		gl.glEnd();
    	    	}
      
 


    
    public void dispose( GLAutoDrawable drawable ) 
    {
    	//final GL2 gl = drawable.getGL().getGL2();
    }
	   
	@Override
    public void init( GLAutoDrawable drawable ) 
    {
		//final GL2 gl = drawable.getGL().getGL2();

	      final GL2 gl = drawable.getGL().getGL2();
	      gl.glShadeModel( GL2.GL_SMOOTH );
	     // for(float i=1f; i>=0f; i=i-0.1f )
	    //  {
	    	  
	      gl.glClearColor( 0.0f, 0f, 0.3f, 0f );
	     // }
	      gl.glClearDepth( 1.0f );
	      gl.glEnable( GL2.GL_DEPTH_TEST );
	      gl.glDepthFunc( GL2.GL_LEQUAL );
	      gl.glHint( GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST );
    }
	
	 @Override
	   public void reshape( GLAutoDrawable drawable, int x, int y, int width, int height ) {
	      final GL2 gl = drawable.getGL().getGL2();
	      if(height ==0)
	         height =1;
	      final float h = ( float ) width / ( float ) height;
	      gl.glViewport( 0, 0, width, height );
	      gl.glMatrixMode( GL2.GL_PROJECTION );
	      gl.glLoadIdentity();
	      glu.gluPerspective( 45.0f, h, 1.0, 20.0 );
	      gl.glMatrixMode( GL2.GL_MODELVIEW );
	      gl.glLoadIdentity();
	   }
    
    public static void main( String[] args ) 
    {
    	final GLProfile profile = GLProfile.get( GLProfile.GL2 );
    	GLCapabilities caps=new GLCapabilities(profile);
    	GLCanvas canvas=new GLCanvas(caps);
    	
    	cart obj=new cart();
    	canvas.addGLEventListener(obj);
    	canvas.setSize(800, 700);
    	
        
    	final JFrame frame=new JFrame("SAMPLE");
    	frame.getContentPane().add( canvas );
  //  	frame.getContentPane().getBackground(Color.blue);
        frame.setSize( frame.getContentPane().getPreferredSize() );
    	//frame.setSize(1500,1500);
    	//frame.add(canvas);
    	frame.setVisible(true);
    	 final FPSAnimator animator = new FPSAnimator( canvas, 300,true );
       animator.start();
    	
    }		
}

	

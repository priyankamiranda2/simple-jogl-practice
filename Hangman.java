package graphics_programs;
import java.awt.*;
import java.awt.event.*; 
import javax.swing.*; 
import java.util.ArrayList;
@SuppressWarnings("serial")
public class Hangman extends JPanel { 
 	private Display display; // The central panel of the GUI, where things are drawn 	 	
 	private ArrayList<JButton> alphabetButtons = new ArrayList<JButton>(); // 26 buttons, with lables "A", "B", ..., "Z" 

 	private JButton nextButton;
 // A button the user can click after one game ends to go on to the next word.
 	private JButton giveUpButton; // A button that the user can click during a game to give up and end the game. 
 	private String message; // A message that is drawn in the Display. 	
 	public String[] wordList={"telephone","world","bag","food","truck","pencil","arrow"};
 	private String word; // The current secret word. 	
 	private String guesses; // A string containing all the letters that the user has guessed so far. 	
 	private boolean gameOver; // False when a game is in progress, true when a game has ended and a new one not yet begun. 	
 	private int badGuesses; // The number of incorrect letters that the user has guessed in the current game. 	 	
 	/** 	 * This class defines a listener that will respond to the events that occur 	 
 	* when the user clicks any of the buttons in the button. The buttons are 	 
 	* labeled "Next word", "Give up", "Quit", "A", "B", "C", ..., "Z". 	 */ 
 	
 	private class ButtonHandler implements ActionListener { 
 	public void actionPerformed( ActionEvent evt ) { 
 	JButton whichButton = (JButton)evt.getSource(); // The button that the user clicked. 			
 	String cmd = evt.getActionCommand(); // The test from the button that the user clicked. 			
 	if (cmd.equals("Quit")) { // Respond to Quit button by ending the program. 				
 	System.exit(0); 			
 	} 	
	else 		
    display.repaint(); // Causes the display to be redrawn, to show any changes made in this method. 		
    } 	

} 	 	
/** 	 * This class defines the panel that occupies the large central area in the 	 
* main panel. The paintComponent() method in this class is responsible for 	 
* drawing the content of that panel. It shows everything that that the user 	 
* is supposed to see, based on the current values of all the instance variables. 	
*/ 	
private class Display extends JPanel { 	
Display() { 		
setPreferredSize(new Dimension(620,420)); 
setBackground( new Color(250, 230, 180) ); 
setFont( new Font("Serif", Font.BOLD, 20) ); 
} 		
protected void paintComponent(Graphics g) { 	
super.paintComponent(g); 	
((Graphics2D)g).setStroke(new BasicStroke(3)); 	
if (message != null) { 			
g.setColor(Color.RED); 			
g.drawString(message, 30, 40); 		
} 	
} 
} 	
public Hangman() { 	
ButtonHandler buttonHandler = new ButtonHandler(); // The ActionListener that will respond to button clicks. 		 	
display = new Display(); // The display panel that fills the large central area of the main panel. 	
JPanel bottom = new JPanel(); // The small panel on the bottom edge of the main panel. 		
setLayout(new BorderLayout(5,5)); // Use a BorderLayout layout manager on the main panel. 		
add(display, BorderLayout.CENTER); // Put display in the central position in the "CENTER" position. 	
add(bottom, BorderLayout.SOUTH); // Put bottom in the "SOUTH" position of the layout. 		
JButton nextButton = new JButton("Next word"); 	
nextButton.addActionListener(buttonHandler); 		
bottom.add(nextButton); 		 		
giveUpButton = new JButton("Give up"); 		
giveUpButton.addActionListener(buttonHandler); 		
bottom.add(giveUpButton); 		 	
JButton quit = new JButton("Quit"); 
quit.addActionListener(buttonHandler); 	
bottom.add(quit); 		
giveUpButton.setEnabled(true); 
for (char alpha='a';alpha<='z';++alpha) { 
alphabetButtons.add(new JButton(String.valueOf(alpha))) ;
} 		
alphabetButtons.add(JButton a);
	
setBackground( new Color(100,0,0) ); 	
setBorder(BorderFactory.createLineBorder(new Color(100,0,0), 3)); 
/* Get the list of possible secret words from the resource file named "wordlist.txt". 		 */ 	
//*************************
//Start the first game. 			
startGame(); 		 	
} 
@SuppressWarnings("null")
private void startGame() { 	
gameOver = false; 	
guesses = ""; 		
badGuesses = 0;


//String word = null;
//word.equals(wordList[2]);	

}
/** 	 * This method can be called to test whether the user has guessed all the letters 	
* in the current secret word. That would mean the user has won the game. 
 */ 	
/*private boolean wordIsComplete() { 
	for (int i = 0; i < word.length(); i++) { 
		char ch = word.charAt(i); 
		if ( guesses.indexOf(ch) == -1 ) { 
			return false; 	
	} 
	} 	
return true; 
}
*/

public static void main(String[] args) { 	
JFrame window = new JFrame("Hangman"); // The window, with "Hangman" in the title bar. 
Hangman panel = new Hangman(); // The main panel for the window. 
window.setContentPane(panel); // Set the main panel to be the content of the window 		
window.pack(); // Set the size of the window based on the preferred sizes of what it contains. 	
window.setResizable(false); // Don't let the user resize the window. 
window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // End the program if the user closes the window. 	
window.setLocation( 100,100); // Position window in the center of screen. 		
window.setVisible(true); // Make the window visible on the screen. 	} } // end class Hangman
} 	 	
 	
}
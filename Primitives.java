package graphics_programs;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import javax.swing.JFrame;


class Primitives implements GLEventListener {

   @Override
   public void display(GLAutoDrawable arg0) {

	   GL2 gl = arg0.getGL().getGL2();
	   gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
       gl.glLoadIdentity();
       gl.glColor3f(0, 0.0f, 0.1f);
       gl.glBegin(GL2.GL_POLYGON);
       gl.glVertex2f(0.7f,-0.5f);
       gl.glVertex2f(-0.7f,-0.5f);
       gl.glVertex2f(-0.7f,0.1f);
       gl.glVertex2f(-0.6f,0.5f);
       gl.glVertex2f(0.6f,0.5f);
       gl.glVertex2f(0.7f,0.1f);
       gl.glVertex2f(0.7f,0.5f);
      
       gl.glEnd();
       
   }
	
   @Override
   public void dispose(GLAutoDrawable arg0) {
     
   }
   private GLU glu;
   @Override
   public void init(GLAutoDrawable arg0) {
	   GL2 gl = arg0.getGL().getGL2();
	    glu = new GLU();
	    gl.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	   
	    
   }
	
   @Override
   public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {

   }
	
   public static void main(String[] args) {
   
      final GLProfile profile = GLProfile.get(GLProfile.GL2);
      GLCapabilities capabilities = new GLCapabilities(profile);
     
      final GLCanvas glcanvas = new GLCanvas(capabilities);
      Primitives b = new Primitives();
      glcanvas.addGLEventListener(b);        
      glcanvas.setSize(400, 400);
     
      final JFrame frame = new JFrame (" Basic Frame");
  
      frame.add(glcanvas);
      frame.setSize( 640, 480 );
      frame.setVisible(true);
   }
	
}

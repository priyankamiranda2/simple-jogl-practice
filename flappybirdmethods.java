package graphics_programs;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLJPanel;
@SuppressWarnings("serial")
public class flappybirdmethods extends JPanel implements ActionListener ,GLEventListener {
	public boolean leftdirection=false;
	public boolean rightdirection=false;
	public boolean GAME=true;
	public int x=50,y=255,x1,y1=0,counter=0;
	private Image basket,johnny;
	 private Timer timer;
	 private GLJPanel drawable;  // The OpenGL display panel.
	 private int frameNumber;    // Current frame number, increases by 1 in each frame.
	 public flappybirdmethods() { 
		    addKeyListener(new bird());
	        setBackground(new Color(0,153,255));
	        setFocusable(true);
	        setPreferredSize(new Dimension(300,300));
	        loadImages();
	        
	        

	        drawable = new GLJPanel();
	        drawable.setPreferredSize(new Dimension(500,500));
	        drawable.addGLEventListener(this);
	        setLayout(new BorderLayout());
	        add(drawable, BorderLayout.BEFORE_FIRST_LINE);
	        Timer timer = new Timer(3, new ActionListener() {
	            public void actionPerformed(ActionEvent evt) {
	                frameNumber++;
	                drawable.repaint();
	            }
	        });
	        timer.setInitialDelay(100);
	        timer.start();
	        
	        
	        
	        
	        
	        
	        
	        
	        
	 }
	 public void paintComponent(Graphics g) {   
	        super.paintComponent(g);    
	        basket(g);
	    }
	 public void loadImages(){
		 ImageIcon iid = new ImageIcon("C:\\Users\\Priyanka Miranda\\workspace1\\graphics_programs\\src\\graphics_programs\\t\\bird.png");
	        basket = iid.getImage();
	        ImageIcon iib = new ImageIcon("C:\\Users\\Priyanka Miranda\\workspace1\\graphics_programs\\src\\graphics_programs\\t\\johnny.png");
	        johnny = iib.getImage();
			 leftdirection=false;
			 rightdirection=false;
			 if(y1==0){
			 int r = (int) (Math.random() * 24);
		         x1= ((r * 10));
			 }
	 }
	 public void basket(Graphics g){
            g.drawImage(johnny,x1,y1,this);
            g.drawImage(basket,x,y , this);
            
            } 
	 private void caught() { 
		 for(int s=0;s<20;s++){
	        if (x==x1+s||x==x1-s) {                                                      
	          y1=0;                                        
	           loadImages();
	           break;
	        }
	        
		 }
	       
	    } 
	    @Override
	    public void actionPerformed(ActionEvent e) {
	    	if (GAME) { 
	    	caught();
	    	move();
	    	drop();
	    	}
	        repaint(); 
	    }
	    private void drop(){
	    	y1=y1+5;
	    }
private void move(){
	if(leftdirection)
	x-=10;
	if(rightdirection)
		x+=10;
}

private class bird extends KeyAdapter { 
    @Override                              
    public void keyPressed(KeyEvent e) { 
    	int key = e.getKeyCode();
        if ((key == KeyEvent.VK_LEFT) ){
            leftdirection = true;  
            rightdirection = false;
                                    
        }
        if ((key == KeyEvent.VK_RIGHT)) {
            rightdirection = true;
            leftdirection = false;
           
        }
       
    }
}































public void init(GLAutoDrawable drawable) {
    GL2 gl2 = drawable.getGL().getGL2();
    gl2.glClearColor(0.5f,0.5f,1,1);
    gl2.glMatrixMode(GL2.GL_PROJECTION);
    gl2.glLoadIdentity();
    gl2.glOrtho(0, 10, 0, 10, 10, 10);
    gl2.glMatrixMode(GL2.GL_MODELVIEW);
}
public void display(GLAutoDrawable drawable) {
    GL2 gl2 = drawable.getGL().getGL2();
    gl2.glClear(GL2.GL_COLOR_BUFFER_BIT); // Fills the scene with blue.
    gl2.glLoadIdentity();
    gl2.glColor3f(0, 0.6f, 0.2f);
    gl2.glBegin(GL2.GL_POLYGON);
    gl2.glVertex2f(-3,-1);
    gl2.glVertex2f(1.5f,1.65f);
    gl2.glVertex2f(5,-1);
    gl2.glEnd();
    gl2.glBegin(GL2.GL_POLYGON);
    gl2.glVertex2f(-3,-1);
    gl2.glVertex2f(3,2.1f);
    gl2.glVertex2f(7,-1);
    gl2.glEnd();
    gl2.glBegin(GL2.GL_POLYGON);
    gl2.glVertex2f(0,-1);
    gl2.glVertex2f(6,1.2f);
    gl2.glVertex2f(20,-1);
    gl2.glEnd();
    gl2.glColor3f(0.4f, 0.4f, 0.5f);
    gl2.glBegin(GL2.GL_POLYGON);
    gl2.glVertex2f(0,-0.4f);
    gl2.glVertex2f(7,-0.4f);
    gl2.glVertex2f(7,0.4f);
    gl2.glVertex2f(0,0.4f);
    gl2.glEnd();
    gl2.glLineWidth(6);  // Set the line width to be 6 pixels.
    gl2.glColor3f(1,1,1);
    gl2.glBegin(GL2.GL_LINES);
    gl2.glVertex2f(0,0);
    gl2.glVertex2f(7,0);
    gl2.glEnd();
    gl2.glLineWidth(1);  // Reset the line width to be 1 pixel.

    /* Draw the sun.  The drawSun method draws the sun centered at (0,0).  A 2D translation
     * is applied to move the center of the sun to (5,3.3).   A rotation makes it rotate*/

    gl2.glPushMatrix();
    gl2.glTranslated(5,3.3,0);
    gl2.glRotated(-frameNumber*0.7,0,0,1);
    drawSun(gl2);
    gl2.glPopMatrix();

    /* Draw three windmills.  The drawWindmill method draws the windmill with its base 
     * at (0,0), and the top of the pole at (0,3).  Each windmill is first scaled to change
     * its size and then translated to move its base to a different paint.  In the animation,
     * the vanes of the windmill rotate.  That rotation is done with a transform inside the
     * drawWindmill method. */

    gl2.glPushMatrix();
    gl2.glTranslated(0.75,1,0);
    gl2.glScaled(0.6,0.6,1);
    gl2.glPopMatrix();

    gl2.glPushMatrix();
    gl2.glTranslated(2.2,1.6,0);
    gl2.glScaled(0.4,0.4,1);
    gl2.glPopMatrix();

    gl2.glPushMatrix();
    gl2.glTranslated(3.7,0.8,0);
    gl2.glScaled(0.7,0.7,1);
    gl2.glPopMatrix();

    /* Draw the cart.  The drawCart method draws the cart with the center of its base at
     * (0,0).  The body of the cart is 5 units long and 2 units high.  A scale is first
     * applied to the cart to make its size more reasonable for the picture.  Then a
     * translation is applied to move the cart horizontally.  The amount of the translation
     * depends on the frame number, which makes the cart move from left to right across
     * the screen as the animation progresses.  The cart animation repeats every 300 
     * frames.  At the beginning of the animation, the cart is off the left edge of the
     * screen. */

    gl2.glPushMatrix();
    gl2.glTranslated(-3 + 13*(frameNumber % 300) / 300.0, 0, 0);
    gl2.glScaled(0.3,0.3,1);
    drawCart(gl2);
    gl2.glPopMatrix();

}

/**
 * Draw a sun with radius 0.5 centered at (0,0).  There are also 13 rays which
 * extend outside from the sun for another 0.25 units.
 */
private void drawSun(GL2 gl2) {
    gl2.glColor3f(0.9f,0.8f,0);
    for (int i = 0; i < 13; i++) { // Draw 13 rays, with different rotations.
        gl2.glRotatef( 360f / 13, 0, 0, 1 ); // Note that the rotations accumulate!
        gl2.glBegin(GL2.GL_LINES);
        gl2.glVertex2f(0, 0);
        gl2.glVertex2f(0.75f, 0);
        gl2.glEnd();
    }
    drawDisk(gl2, 0.5);
    gl2.glColor3f(0,0,0);
}

/**
 * Draw a 32-sided regular polygon as an approximation for a circular disk.
 * (This is necessary since OpenGL has no commands for drawing ovals, circles,
 * or curves.)  The disk is centered at (0,0) with a radius given by the 
 * parameter.
 */
private void drawDisk(GL2 gl2, double radius) {
    gl2.glBegin(GL2.GL_POLYGON);
    for (int d = 0; d < 32; d++) {
        double angle = 2*Math.PI/32 * d;
        gl2.glVertex2d( radius*Math.cos(angle), radius*Math.sin(angle));
    }
    gl2.glEnd();
}


/**
 * Draw a cart consisting of a rectangular body and two wheels.  The wheels
 * are drawn by the drawWheel() method; a different translation is applied to each
 * wheel to move them into position under the body.  The body of the cart
 * is a red rectangle with corner at (0,-2.5), width 5, and height 2.  The
 * center of the bottom of the rectangle is at (0,0).
 */
private void drawCart(GL2 gl2) {
    gl2.glPushMatrix();
    gl2.glTranslatef(-1.5f, -0.1f, 0);
    gl2.glScalef(0.8f,0.8f,1);
    drawWheel(gl2);
    gl2.glPopMatrix();
    gl2.glPushMatrix();
    gl2.glTranslatef(1.5f, -0.1f, 0);
    gl2.glScalef(0.8f,0.8f,1);
    drawWheel(gl2);
    gl2.glPopMatrix();
    gl2.glColor3f(1,0,0);
    gl2.glBegin(GL2.GL_POLYGON);
    gl2.glVertex2f(-2.5f,0);
    gl2.glVertex2f(2.5f,0);
    gl2.glVertex2f(2.5f,2);
    gl2.glVertex2f(-2.5f,2);
    gl2.glEnd();
}

/**
 * Draw a wheel, centered at (0,0) and with radius 1. The wheel has 15 spokes
 * that rotate in a clockwise direction as the animation proceeds.
 */
private void drawWheel(GL2 gl2) {
    gl2.glColor3f(0,0,0);
    drawDisk(gl2,1);
    gl2.glColor3f(0.75f, 0.75f, 0.75f);
    drawDisk(gl2, 0.8);
    gl2.glColor3f(0,0,0);
    drawDisk(gl2, 0.2);
    gl2.glRotatef(frameNumber*20,0,0,1);
    gl2.glBegin(GL2.GL_LINES);
    for (int i = 0; i < 15; i++) {
        gl2.glVertex2f(0,0);
        gl2.glVertex2d(Math.cos(i*2*Math.PI/15), Math.sin(i*2*Math.PI/15));
    }
    gl2.glEnd();
}

public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
}

public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
}

public void dispose(GLAutoDrawable arg0) {
}
}
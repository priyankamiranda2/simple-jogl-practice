package graphics_programs;
import java.awt.*;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
@SuppressWarnings("serial")
public class Project1 extends JPanel{
	int a=10;
	int b=10;
	int c=10;
	int d=10;
	public static void main(String main[]){
JFrame window= new JFrame("Button!");
window.setContentPane(new Project1());//we go to the constructor with the default properties required
window.pack();//this one makes sure that the size of every element is in order of the window size
window.setResizable(true);
window.setLocation(50, 50);
window.setVisible(true);
window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public Project1(){
	setPreferredSize( new Dimension(500,500));	
	setBorder(BorderFactory.createDashedBorder(Color.yellow));
	setLayout(null);
	JButton button= new JButton("Bigger!");
	button.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			
		 c=2*c;
		 d=2*d;
			
		repaint();	
		}
	});
	button.setBounds(400,100,120,30);
	add(button);
	}
	
	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		setBackground(Color.BLACK);
		g.setColor(Color.WHITE);
		g.fillRect(a, b, c, d);
	}
}

package graphics_programs;
public class interfaces { 
    public static void main(String[] args) {
        e1 employee = new employee();
        employee.Display();
    }
} 
interface e1{
    public String name = "";
    public void Display();
}
interface e2 extends e1{
    public String name = "shape2";
    public void Display2();
}
interface e3 {
    public String name = "shape3";
    public void Display3();
}
class employee implements e1,e2,e3 {
    public void DrawDisplay() {
        System.out.println("Name of first employee:" + details.name);
    }
    @Override
    public void Display2() {
        System.out.println("Drawing Circle here:" + moredetails.name);
    }
    @Override
    public void Display3() {
        System.out.println("Drawing Circle here:" + marks.name);
    }
}
package graphics_programs;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class SecondJavaGraphics extends JPanel {
	public static void main(String[] args)//main method executes first
	{
		SecondJavaGraphics fjg=new SecondJavaGraphics();//creating an object of the same class
		JFrame window=new JFrame("Hello Java Graphics");//JFrame is a method in class JPanel
		window.setSize(640, 480);//window is the object to the method i created. Now i set the windows size
		window.add(fjg);//we next add the object of SecondJavaGraphics in the window
		window.setVisible(true);//we make the window visible
	}
	public void paintComponent(Graphics g)//this method executes when you call fjg object of the class in main method
	{
		super.paintComponent(g);//this gets the method paint component from the base class JPanel
		  g.setColor(Color.darkGray);
          g.drawLine(0,0,30,30);
         
          g.setColor(Color.red);
          g.fillRect(40,40,40,40);
          g.setColor(Color.red);
          g.fillRect(120,40,40,40);
         
          g.setColor(Color.green);
          g.fillRect(80,80,40,40);
          g.setColor(Color.green);
          g.fillRect(160,80,40,40);
         
         
	}

}

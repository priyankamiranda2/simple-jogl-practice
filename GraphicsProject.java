package graphics_programs;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.geom.*;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.util.FPSAnimator;
//import java.awt.geom.*;
import com.jogamp.opengl.GLEventListener;
@SuppressWarnings("serial")
public class GraphicsProject implements GLEventListener{

    public void dispose(GLAutoDrawable a)

    {

        

    }

    public void init(GLAutoDrawable a)

    {

         GL2 ob=a.getGL().getGL2();

        //float 78651521 i=0;

         ob.glClearColor(0.7f,0.7f,0.5f,0.f);

            // i=i-0.1;

    }

    /* private void drawWheel(GL2 ob) {

        ob.glColor3f(0,0,0);

        drawDisk(ob,1);

      ob.glColor3f(0.75f, 0.75f, 0.75f);

        drawDisk(ob, 0.8);

      ob.glColor3f(0,0,0);

        drawDisk(ob, 0.2);

        ob.glRotatef(frameNumber*20,0,0,1);

       ob.glBegin(GL2.GL_LINES);

        for (int i = 0; i < 15; i++) {

            ob.glVertex2f(0,0);

            ob.glVertex2d(Math.cos(i*2*Math.PI/15), Math.sin(i*2*Math.PI/15));

        }

        ob.glEnd();

     }*/

    double x,y;

    

    void w1(GL2 ob)//wheel1

    {

        

        ob.glBegin(GL2.GL_POLYGON);

        ob.glColor3f(1,1,1);

        for(int i=0;i<360;i++)

        {

            x=0.025*Math.cos(i);

            y=0.025*Math.sin(i);

            ob.glVertex2d(x,y);

        }

        ob.glEnd();

        

    }

     public void reshape(GLAutoDrawable a,int b,int c,int d,int e)

    {

         

    }

    float i=0;

    public void display(GLAutoDrawable a)

    {

        

        GL2 ob = a.getGL().getGL2();

        ob.glClear(GL2.GL_COLOR_BUFFER_BIT);

         ob.glLoadIdentity();

        //---------------------------------------------------

       

         ob.glBegin(GL2.GL_POLYGON);//BLACKWAY

         ob.glColor3f(0.50f,0.0f,0.5f);

         

        ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glVertex3f(1.0f,-0.65f,0.0f);

        ob.glVertex3f(1.0f,-0.7f,0.0f);

        ob.glVertex3f(-1.0f,-0.7f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        //---------------------------------------------------

        ob.glBegin(GL2.GL_POLYGON);//ROAD

         ob.glColor3f(0.8f,0.8f,0.8f);

         

        ob.glVertex3f(-1.0f,-0.7f,0.0f);

        ob.glVertex3f(1.0f,-0.7f,0.0f);

        ob.glVertex3f(1.0f,-1.0f,0.0f);

        ob.glVertex3f(-1.0f,-1.0f,0.0f);

      

        ob.glEnd();

        

        //----------------------------------------------------

         ob.glBegin(GL2.GL_POLYGON);//SHADOW1

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(-1.0f,0.3f,0.0f);

        ob.glVertex3f(-0.60f,0.3f,0.0f);

        ob.glVertex3f(-0.60f,-0.65f,0.0f);

        ob.glVertex3f(-1.0f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//SHADOW2

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(-0.60f,0.5f,0.0f);

        ob.glVertex3f(-0.40f,0.5f,0.0f);

        ob.glVertex3f(-0.40f,-0.65f,0.0f);

        ob.glVertex3f(-0.60f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//SHADOW3

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(-0.40f,0.6f,0.0f);

        ob.glVertex3f(-0.3f,0.6f,0.0f);

        ob.glVertex3f(-0.30f,-0.65f,0.0f);

        ob.glVertex3f(-0.4f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//SHADOW4

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(-0.3f,0.4f,0.0f);

        ob.glVertex3f(-0.2f,0.4f,0.0f);

        ob.glVertex3f(-0.20f,-0.65f,0.0f);

        ob.glVertex3f(-0.3f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//SHADOW5

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(-0.20f,0.3f,0.0f);

        ob.glVertex3f(-0.10f,0.3f,0.0f);

        ob.glVertex3f(-0.10f,-0.65f,0.0f);

        ob.glVertex3f(-0.20f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//SHADOW6

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(-0.1f,0.4f,0.0f);

        ob.glVertex3f(0.20f,0.4f,0.0f);

        ob.glVertex3f(0.20f,-0.65f,0.0f);

        ob.glVertex3f(-0.10f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        ob.glBegin(GL2.GL_POLYGON);//SHADOW7

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(0.0f,0.4f,0.0f);

        ob.glVertex3f(0.4f,0.4f,0.0f);

        ob.glVertex3f(0.40f,-0.65f,0.0f);

        ob.glVertex3f(0.00f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        ob.glBegin(GL2.GL_POLYGON);//SHADOW8

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(0.4f,0.6f,0.0f);

        ob.glVertex3f(0.60f,0.6f,0.0f);

        ob.glVertex3f(0.60f,-0.65f,0.0f);

        ob.glVertex3f(0.4f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//SHADOW9

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(0.6f,0.4f,0.0f);

        ob.glVertex3f(0.80f,0.4f,0.0f);

        ob.glVertex3f(0.80f,-0.65f,0.0f);

        ob.glVertex3f(0.60f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

   

         ob.glBegin(GL2.GL_POLYGON);//SHADOW10

         ob.glColor3f(0.5f,0.5f,0.5f);

         

        ob.glVertex3f(0.80f,0.4f,0.0f);

        ob.glVertex3f(1.0f,0.4f,0.0f);

        ob.glVertex3f(1.0f,-0.65f,0.0f);

        ob.glVertex3f(0.80f,-0.65f,0.0f);

        //ob.glVertex3f(-1.0f,-0.65f,0.0f);

        ob.glEnd();

        

        //--------------------------------------------------

         ob.glBegin(GL2.GL_POLYGON);//Building1

         ob.glColor3f(0.5f,0.5f,0.0f);

         

        ob.glVertex3f(-1.0f,0.2f,0.0f);

        ob.glVertex3f(-0.7f,0.2f,0.0f);

        ob.glVertex3f(-0.7f,-0.65f,0.0f);

        ob.glVertex3f(-1.00f,-0.65f,0.0f);

        

        ob.glEnd();

       

         

        

        ob.glBegin(GL2.GL_POLYGON);//Building2

         ob.glColor3f(0.0f,0.9f,1.0f);

         

        ob.glVertex3f(-0.53f,0.3f,0.0f);

        ob.glVertex3f(-0.3f,0.3f,0.0f);

        ob.glVertex3f(-0.3f,-0.65f,0.0f);

        ob.glVertex3f(-0.53f,-0.65f,0.0f);

        

        ob.glEnd();

         ob.glBegin(GL2.GL_TRIANGLES);//Building2 part 2

         ob.glColor3f(0.0f,0.9f,1.0f);

         

        ob.glVertex3f(-0.53f,0.3f,0.0f);

        ob.glVertex3f(-0.53f,0.4f,0.0f);

        ob.glVertex3f(-0.3f,0.3f,0.0f);

        

        ob.glEnd();

        //=========-----

        ob.glBegin(GL2.GL_POLYGON);//Building3 part1

         ob.glColor3f(1.0f,1.0f,0.0f);

         

        ob.glVertex3f(-0.70f,0.0f,0.0f);

        ob.glVertex3f(-0.65f,0.0f,0.0f);

        ob.glVertex3f(-0.65f,-0.65f,0.0f);

        ob.glVertex3f(-0.70f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Building3 part2

         ob.glColor3f(1.0f,1.0f,0.0f);

         

        ob.glVertex3f(-0.650f,0.3f,0.0f);

        ob.glVertex3f(-0.5f,0.3f,0.0f);

        ob.glVertex3f(-0.5f,-0.65f,0.0f);

        ob.glVertex3f(-0.65f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Building3 part3

         ob.glColor3f(1.0f,1.0f,0.0f);

         

        ob.glVertex3f(-0.6f,0.6f,0.0f);

        ob.glVertex3f(-0.55f,0.6f,0.0f);

        ob.glVertex3f(-0.55f,-0.65f,0.0f);

        ob.glVertex3f(-0.6f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Building3 part4

         ob.glColor3f(1.0f,1.0f,0.0f);

         

        ob.glVertex3f(-0.50f,0.0f,0.0f);

        ob.glVertex3f(-0.45f,0.0f,0.0f);

        ob.glVertex3f(-0.45f,-0.65f,0.0f);

        ob.glVertex3f(-0.50f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_TRIANGLES);//Building3 part5

         ob.glColor3f(0.94f,0.81f,0.99f);

         

        ob.glVertex3f(-0.60f,0.6f,0.0f);

        ob.glVertex3f(-0.575f,0.8f,0.0f);

        ob.glVertex3f(-0.55f,0.6f,0.0f);

        

        ob.glEnd();

    

        //----------------

        

        ob.glBegin(GL2.GL_POLYGON);//Building4

         ob.glColor3f(0.0f,0.0f,0.5f);

         

        ob.glVertex3f(-0.30f,0.2f,0.0f);

        ob.glVertex3f(0.0f,0.2f,0.0f);

        ob.glVertex3f(0.0f,-0.65f,0.0f);

        ob.glVertex3f(-0.30f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Building5

         ob.glColor3f(0.5f,0.2f,0.1f);

         

        ob.glVertex3f(0.0f,0.5f,0.0f);

        ob.glVertex3f(0.2f,0.5f,0.0f);

        ob.glVertex3f(0.2f,-0.65f,0.0f);

        ob.glVertex3f(0.0f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Building6

         ob.glColor3f(1.0f,0.650f,0.0f);

         

        ob.glVertex3f(0.2f,0.4f,0.0f);

        ob.glVertex3f(0.4f,0.4f,0.0f);

        ob.glVertex3f(0.4f,-0.65f,0.0f);

        ob.glVertex3f(0.2f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Building7

         ob.glColor3f(1.0f,0.1f,0.1f);

         

        ob.glVertex3f(0.40f,0.3f,0.0f);

        ob.glVertex3f(0.6f,0.3f,0.0f);

        ob.glVertex3f(0.6f,-0.65f,0.0f);

        ob.glVertex3f(0.40f,-0.65f,0.0f);

        

        ob.glEnd();

        

         ob.glBegin(GL2.GL_POLYGON);//Building8

         ob.glColor3f(0.70f,0.0f,0.0f);

         

        ob.glVertex3f(0.60f,0.7f,0.0f);

        ob.glVertex3f(0.8f,0.7f,0.0f);

        ob.glVertex3f(0.8f,-0.65f,0.0f);

        ob.glVertex3f(0.6f,-0.65f,0.0f);

        

        ob.glEnd();

         ob.glBegin(GL2.GL_POLYGON);//Building9

         ob.glColor3f(1.0f,0.0f,1.0f);

         

        ob.glVertex3f(0.80f,0.2f,0.0f);

        ob.glVertex3f(1.0f,0.2f,0.0f);

        ob.glVertex3f(1.0f,-0.65f,0.0f);

        ob.glVertex3f(0.80f,-0.65f,0.0f);

        

        ob.glEnd();

        

        ob.glTranslatef(i,0.0f,0.0f);

        ob.glBegin(GL2.GL_POLYGON);//Car(rect)

        ob.glColor3f(0.0f,0.0f,0.50f);

         

        ob.glVertex3f(0.70f,-0.98f,0.0f);

        ob.glVertex3f(1.0f,-0.98f,0.0f);

        ob.glVertex3f(1.0f,-0.88f,0.0f);

       // ob.glVertex3f(0.95f,-0.88f,0.0f);

        //ob.glVertex3f(0.90f,-0.85f,0.0f);

        //ob.glVertex3f(0.60f,-0.85f,0.0f);

        //ob.glVertex3f(0.75f,-0.88f,0.0f);

        ob.glVertex3f(0.70f,-0.88f,0.0f); 

        ob.glEnd();

        

        ob.glBegin(GL2.GL_POLYGON);//Car(top)

        ob.glColor3f(0.0f,0.0f,0.50f);

        ob.glVertex3f(0.95f,-0.88f,0.0f);

        ob.glVertex3f(0.95f,-0.85f,0.0f);

        ob.glVertex3f(0.75f,-0.85f,0.0f);

        ob.glVertex3f(0.75f,-0.88f,0.0f);

        ob.glEnd();

        ob.glPushMatrix();

        ob.glTranslatef(0.75f,-0.97f,0.00f);

        w1(ob);

        ob.glPopMatrix();

        ob.glTranslatef(0.95f,-0.97f,0.00f);

         w1(ob);

        i=i-0.05f;

        /*ob.glBegin(GL2.GL_POLYGON);//Building 0

         ob.glColor3f(0.5f,0.5f,0.0f);

 

        ob.glVertex3f(-1.0f,0.2f,0.0f);

        ob.glVertex3f(-0.90f,0.2f,0.0f);

        ob.glVertex3f(-0.90f,-0.65f,0.0f);

        ob.glVertex3f(-1.0f,-0.65f,0.0f);

        

        ob.glEnd();*/

        

       

    }

    




    /**

     * @param args the command line arguments

     */

    public static void main(String[] args) {


        final GLProfile profile=GLProfile.get(GLProfile.GL2);

        GLCapabilities capabilities=new GLCapabilities(profile);

        final GLCanvas glcanvas=new GLCanvas(capabilities);

        FPSAnimator anime=new FPSAnimator(glcanvas,5,true);

        GraphicsProject t=new GraphicsProject();

        glcanvas.addGLEventListener(t);

        glcanvas.setSize(400,400);

        final JFrame frame=new JFrame("CG Project:");

        frame.add(glcanvas);

        frame.setSize(640,480);

        frame.setVisible(true);

        anime.start();

    }
}

   


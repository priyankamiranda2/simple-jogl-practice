package graphics_programs;
import java.awt.EventQueue;
import javax.swing.JFrame;
@SuppressWarnings("serial")
public class flappy_bird extends JFrame {
    public flappy_bird() { 
        add(new flappybirdmethods());   
        setResizable(true); 
        pack();
        setTitle("Flappy bird");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {                
                JFrame window = new flappy_bird(); 
                window.setVisible(true);            
            }
        });
    }
}
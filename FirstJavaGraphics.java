	package graphics_programs;
/* Program to display a rectangle, filled rectangle and a line all with different colors
 * and a text string
 */
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;
//import javax.swing.Timer;
@SuppressWarnings("serial")
public class FirstJavaGraphics extends JPanel
{
	
public static void main(String[] args)
{
	FirstJavaGraphics fjg=new FirstJavaGraphics();
	JFrame window=new JFrame("Hello Java Graphics");
	window.setSize(640, 480);
	window.add(fjg);
	window.setVisible(true);
}
public void paintComponent(Graphics g)
{
	super.paintComponent(g);
	setBackground(Color.PINK);
	g.drawRect(100, 20, 30, 30);
	g.setColor(Color.RED);
	g.fillRect(200, 320, 40, 40);
	g.drawLine(100, 100, 200, 200);
	g.setColor(Color.BLUE);
	g.drawString("Hello World", 100, 250);
}
FirstJavaGraphics()
{
	
}
}
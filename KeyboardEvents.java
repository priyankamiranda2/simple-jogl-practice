package graphics_programs;

import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Label;
import javax.swing.JTextField;
 
/*
 * To create a stand alone window, class should be extended from
 * Frame and not from Applet class.
 */
@SuppressWarnings("serial")
public class KeyboardEvents extends Frame{
	String msg="";
	int x=10,y=20;
	private Label statusLabel;

       KeyboardEvents(String title){
                //call the superclass constructor
                super();
                //set window title using setTitle method
                this.setTitle(title);
                /*
                 * Newly created window will not be displayed until we call
                 * setVisible(true).
                 */
                this.setSize(600, 400);
               
                statusLabel = new Label();        
               statusLabel.setAlignment(Label.CENTER);
              statusLabel.setSize(350,100);
                
                final JTextField textField = new JTextField(10);

                textField.addKeyListener(new KeyAdapter() {
                   public void keyPressed(KeyEvent e) {                
                      if(e.getKeyCode() == KeyEvent.VK_ENTER){
                         statusLabel.setText("Entered text: " + textField.getText());
                      }
                   }        
                });
                this.add(statusLabel);
                this.add(textField);
                this.setVisible(true);
        }
       
        public static void main(String args[]){
                final KeyboardEvents window =
                                new KeyboardEvents("Keyboard events Example");
                window.addWindowListener(new WindowAdapter() {
            		public void windowClosing(WindowEvent e) {
            			window.dispose();
            			System.exit(0);
            		}
            });
               window.requestFocus();
        }
}
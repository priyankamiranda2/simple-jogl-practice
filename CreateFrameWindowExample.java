package graphics_programs;

import java.awt.Frame;

/*
 * To create a stand alone window, class should be extended from
 * Frame and not from Applet class.
 */
@SuppressWarnings("serial")
public class CreateFrameWindowExample extends Frame{
 
        CreateFrameWindowExample(String title){
               
                super();
                this.setTitle("trying to figure out graphics");
                this.setVisible(false);
        }
       
        public static void main(String args[]){
   CreateFrameWindowExample window = new CreateFrameWindowExample("Create Window Example");
                /*
                 * In order to close the window, we will have to handle the events
                 * and call setVisible(false) method.
                 *
                 * This Example program does not handle events, so you will have
                 * to terminate the program (by pressing ctrl+C) in a terminal window
                 * to close the frame window.
                 */
        }
}